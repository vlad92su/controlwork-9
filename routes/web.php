<?php

use App\Http\Controllers\Admin\CommentController;
use App\Http\Controllers\Admin\HomeController;
use App\Http\Controllers\CommentsController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\RatingsController;
use App\Http\Controllers\UsersController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::match(['get', 'post'], '/', [NewsController::class, 'index'])->name('home');
Route::resource('news', NewsController::class)->only('index','create', 'store','show');
Route::post('ratings', [RatingsController::class, 'store'])->name('store-rating')->middleware( 'auth');
Route::post('comments', [CommentsController::class, 'store'])->name('comments-store')->middleware( 'auth');
Route::get('users/{user}', [UsersController::class, 'show'])->name('show-user');


Route::prefix('admin')->middleware( 'auth')->name('admin.')->group(function () {
    Route::get('/', [HomeController::class, 'index'])->name('admin-profile');
    Route::resource('news' ,App\Http\Controllers\Admin\NewsController::class)->except('show');
    Route::resource('comments' ,App\Http\Controllers\Admin\CommentController::class)->except('create','store','show');
    Route::post('comments-approve/{comment}', [CommentController::class, 'approve'])->name('comments-approve');
});

Auth::routes();

