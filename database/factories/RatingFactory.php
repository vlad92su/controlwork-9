<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Rating>
 */
class RatingFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'quality' => rand(-5, 5),
            'relevance' => rand(-5, 5),
            'satisfied' => rand(0, 1),
            'news_id' => rand(1, 30),
            'user_id' => rand(1, 5),
        ];
    }
}
