<?php

namespace Database\Factories;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\News>
 */
class NewsFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'text' => $this->faker->paragraph(3),
            'tag_id' => rand(1, 8),
            'category_id' => rand(1, 5),
            'user_id' => rand(1, 5),
            'publication_date' => $this->faker->dateTimeBetween(),
        ];
    }
}
