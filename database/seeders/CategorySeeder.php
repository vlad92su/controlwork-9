<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('categories')->insert([
            'name' => 'Policy'
        ]);
        DB::table('categories')->insert([
            'name' => 'Science'
        ]);
        DB::table('categories')->insert([
            'name' => 'History'
        ]);
        DB::table('categories')->insert([
            'name' => 'Sport'
        ]);
        DB::table('categories')->insert([
            'name' => 'Economy'
        ]);
    }
}
