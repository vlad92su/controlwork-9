<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        DB::table('tags')->insert([
            'tag' => 'News_of_the_week'
        ]);
        DB::table('tags')->insert([
            'tag' => 'Family'
        ]);
        DB::table('tags')->insert([
            'tag' => 'Football'
        ]);
        DB::table('tags')->insert([
            'tag' => 'News_from_the_roads'
        ]);
        DB::table('tags')->insert([
            'tag' => 'Cyber_news'
        ]);
        DB::table('tags')->insert([
            'tag' => 'Auto_sport'
        ]);
        DB::table('tags')->insert([
            'tag' => 'Air_news'
        ]);
        DB::table('tags')->insert([
            'tag' => 'Main_news'
        ]);

    }
}
