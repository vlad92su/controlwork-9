<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use App\Models\Comment;
use App\Models\News;
use App\Models\Rating;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        \App\Models\User::factory(5)->create();

        DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('qwe'),
            'is_admin' => true
        ]);

        $this->call(CategorySeeder::class);
        $this->call(TagSeeder::class);

        News::factory()->count(30)
            ->has(Comment::factory()->count(5))->create();

        \App\Models\Rating::factory(20)->create();



    }
}
