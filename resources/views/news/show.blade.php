@extends('layouts.app')
@section('content')

    <div class="container mt-5">
        <div class="d-flex">
            <div class="col-lg-6">
                <div style="padding-top: 70px; height: 65vh">
                    <div>
                        <h2 style="text-align: center">News</h2>
                        <p style="padding-top: 10px;">{{ $news->text }}</p>
                    </div>

                    @if(Auth::check())
                        <form method="post" action="{{ route('store-rating') }}">
                            @csrf
                            <input type="hidden" name="news_id" value="{{ $news->id }}">
                            <div>
                                <h3 style="text-align: center">Rate the news</h3>
                                <div class="row">
                                    <div class="col-4">
                                        <select name="quality" class="form-select" aria-label="Default select example">
                                            <option value=" " selected>Quality control</option>
                                            @for($i = -5; $i <= 5; $i++)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                        @error('quality')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>
                                    <div class="col-4">
                                        <select name="relevance" class="form-select"
                                                aria-label="Default select example">
                                            <option value=" " selected>Relevance control</option>
                                            @for($i = -5; $i <= 5; $i++)
                                                <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                        </select>
                                        @error('relevance')
                                        <div class="alert alert-danger">{{$message}}</div>
                                        @enderror
                                    </div>

                                    <div class="col-4">
                                        <input class="form-check-input" type="checkbox" value="0" name="satisfied"
                                               id="flexCheckDefault">
                                        <label class="form-check-label" for="flexCheckDefault">Satisfied</label>
                                    </div>
                                    <div class="mt-3" style="width: 15%;margin: 0 auto;">
                                        <button type="submit" style="padding: 8px 50px; color: black"
                                                class="btn btn-primary">Save
                                        </button>
                                    </div>
                                    @error('satisfied')
                                    <div class="alert alert-danger">{{$message}}</div>
                                    @enderror
                                </div>
                            </div>
                        </form>
                    @endif
                </div>
            </div>
            <div class="col-lg-6" style="text-align: center; padding: 0px 20px; ">
                <h1><b>Comments</b></h1>
                @if(Auth::check())
                    <form method="post" action="{{ route('comments-store') }}" id="create-comment">
                        <input type="hidden" name="news_id" value="{{ $news->id }}">
                        @csrf
                        <div style="display: flex; align-items: start; gap: 10px;justify-content: center;">
                            <lable for="text-comment">Text</lable>
                            <textarea name="comment" id="text-comment"
                                      style="border-radius: 5px; border: none;width: 350px;"></textarea>
                        </div>
                        @error('comments')
                        <div class="alert alert-danger mt-2"
                             style="display: flex; align-items: start; gap: 10px;justify-content: center;">{{$message}}</div>
                        @enderror

                        <div class="mt-3">
                            <button type="submit"
                                    style="padding: 7px 15px; border-radius: 10px; border: none;margin-bottom: 20px;">
                                add
                                comment
                            </button>
                        </div>
                    </form>
                @endif

                @foreach($news->comments as $comment)
                    @csrf
                    @if($comment->approve)
                        <div>
                            <div style="text-align: start;padding-left: 60px;">
                                <p class="m-0 p-0">By: <span>{{ $comment->user->name }} </span>
                                <p>{{ $comment->comment }}</p>
                            </div>
                            <hr>
                        </div>
                    @endif
                @endforeach
            </div>
        </div>
    </div>

@endsection

