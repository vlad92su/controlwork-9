@extends('layouts.app')
@section('content')

    <div class="container mt-5">
        <form method="post" action="{{ route('home') }}">
            @csrf
            <div class="row mb-5">
                <div class="col-5">
                    <select class="form-select" name="category" aria-label="Default select example">
                        <option selected>Filter by category</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-5">
                    <select class="form-select" name="tag" aria-label="Default select example">
                        <option selected>Filter by tag</option>
                        @foreach($tags as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->tag }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-2">
                    <button type="submit" style="padding: 8px 50px; color: black" class="btn btn-primary">Search
                    </button>
                </div>
            </div>
        </form>

        <div class="row">
            @foreach($news as $item)
                <div class="col-3 mb-2">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <p class="card-text"><a
                                    href="{{ route('news.show', ['news' => $item]) }}">{{ $item->text }}</a></p>
                            <span>#{{ $item->tag->tag }}</span>
                            <p style="text-align: start"><a
                                    href="{{route('show-user', ['user' => $item->user])}}"><span>By: </span>{{$item->user->name}}
                                </a></p>
                            <p style="text-align: end">Publication
                                date: {{ date_format(new DateTime($item->publication_date), 'd-m-y') }} y.</p>
                            <p style="text-align: end">Created
                                date: {{ date_format(new DateTime($item->created_at), 'd-m-y') }} y.</p>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

        <div class="justify-content-md-center p-5">
            <div class="col-md-auto">
                {{ $news->links() }}
            </div>
        </div>
    </div>


@endsection
