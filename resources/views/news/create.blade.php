@extends('layouts.app')
@section('content')
    <div class="container mt-5">
        <div style="width: 80%; margin: 0 auto">
            <h2 class="mt-5">Create new news</h2>
            <form method="post" action="{{ route('news.store') }}" enctype="multipart/form-data">
                @csrf
                <div class="mb-3 mt-5" style="display: flex; align-items: start; gap: 10px;justify-content: center;">
                    <textarea type="text" name="text" class="form-control" placeholder="News text"
                              aria-label="news_text"
                              aria-describedby="addon-wrapping"></textarea>
                </div>
                @error('text')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror

                <div class="mb-3" style="display: flex; align-items: start; gap: 10px;justify-content: center;">
                    <select name="category_id" class="form-select" aria-label="Default select example">
                        <option value=" " selected>Choose category</option>
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                @error('category_id')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror

                <div class="mb-3" style="display: flex; align-items: start; gap: 10px;justify-content: center;">
                    <select name="tag_id" class="form-select" aria-label="Default select example">
                        <option value=" " selected>Choose tag</option>
                        @foreach($tags as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->tag }}</option>
                        @endforeach
                    </select>
                </div>
                @error('tag_id')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror

                <div style="width: 15%;margin: 0 auto;">
                    <button type="submit" style="padding: 8px 50px; color: black" class="btn btn-primary">Save</button>
                </div>
            </form>
        </div>
    </div>

@endsection

