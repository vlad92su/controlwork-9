@extends('layouts.app')
@section('content')

    <div class="container mt-5">
        <div class="col-12 d-flex" style="gap: 25px">
            <h4><b>Name: </b>{{ $user->name }} profile</h4>
            <h4><b>Email: </b>{{ $user->email }}</h4>
            @if(Auth::check() && Auth::user()->id == $user->id)
                <a href="{{ route('news.create') }}" class="btn btn-primary" style="padding: 8px 30px;color: black">Create
                    new news</a>
            @endif
        </div>

        <div>
            <h4><b>Rating: </b>{{ $rating_news}} </h4>
            <h5>Number of published news: {{ $count_public_news }}</h5>
            <h5>Overall news quality: {{ $count_quality }}</h5>
            <h5>Total news relevance: {{ $count_relevance }}</h5>
        </div>
    </div>

@endsection
