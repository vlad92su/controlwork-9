@extends('layouts.app')
@section('content')
    <div class="container mt-5">
        <div>
            <a href="{{ route('admin.news.create') }}" class="btn btn-primary" style="padding: 8px 30px;color: black">Create
                new news</a>
        </div>
        <div class="mt-3">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Text</th>
                    <th scope="col">Publication date</th>
                    <th scope="col">Created date</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($news as $item)
                    <tr>
                        <th scope="row">{{ $item->id }}</th>
                        <td>{{ $item->text }}</td>
                        @if($item->publication_date)
                            <td>{{ date_format(new DateTime($item->publication_date), 'd-m-y')  }}</td>
                        @else
                            <td></td>
                        @endif
                        <td>{{ date_format(new DateTime($item->created_at), 'd-m-y')  }}</td>
                        <td>

                            <div class="row">
                                <div class="col-6 box-btn">
                                    <a class="btn btn-outline-warning"
                                       href="{{ route('admin.news.edit', ['news' => $item]) }}">Edit
                                    </a>
                                    <form style="float: left; margin-right: 12px;"
                                          action="{{ route('admin.news.destroy', ['news' => $item]) }}" method="post">
                                        @method('delete')
                                        @csrf
                                        <button class="btn btn-danger btn-sm active">Remove</button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="justify-content-md-center p-5">
            <div class="col-md-auto">
                {{ $news->links() }}
            </div>
        </div>

    </div>

@endsection


