@extends('layouts.app')
@section('content')
    <div class="container mt-5">
        <div style="width: 80%; margin: 0 auto">
            <h2 class="mt-5">Update news</h2>
            <form method="post" action="{{ route('admin.news.update', ['news' => $news]) }}"
                  enctype="multipart/form-data">
                @method('put')
                @csrf
                <div class="mb-3 mt-5" style="display: flex; align-items: start; gap: 10px;justify-content: center;">
                    <textarea type="text" name="text" class="form-control" placeholder="News text"
                              aria-label="news_text"
                              aria-describedby="addon-wrapping">{{ $news->text }}</textarea>
                </div>
                @error('text')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror

                <div class="mb-3" style="display: flex; align-items: start; gap: 10px;justify-content: center;">
                    <select name="category_id" class="form-select" aria-label="Default select example">
                        @if(is_null($news->category_id))
                            <option value="{{ $news->category_id }}" selected>{{ $news->category->name }}</option>
                        @endif
                        @foreach($categories as $category)
                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                        @endforeach
                    </select>
                </div>
                @error('category_id')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror

                <div class="mb-3" style="display: flex; align-items: start; gap: 10px;justify-content: center;">
                    <select name="tag_id" class="form-select" aria-label="Default select example">
                        @if(is_null($news->tag_id))
                            <option value="{{ $news->tag_id }}" selected>{{ $news->tag->tag }}</option>
                        @endif
                        @foreach($tags as $tag)
                            <option value="{{ $tag->id }}">{{ $tag->tag }}</option>
                        @endforeach
                    </select>
                </div>
                @error('tag_id')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror

                <div class="mb-3" style="display: flex; align-items: start; gap: 10px;justify-content: center;">
                    <input name="publication_date" class="form-select" aria-label="Default select example" type="date">
                </div>

                <div style="width: 15%;margin: 0 auto;">
                    <button type="submit" style="padding: 8px 50px; color: black" class="btn btn-primary">Update
                    </button>
                </div>
            </form>
        </div>
    </div>

@endsection

