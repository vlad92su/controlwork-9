@extends('layouts.app')
@section('content')
    <div class="container mt-5">
        <div class="mt-3">
            <table class="table table-striped table-hover">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Comment</th>
                    <th scope="col">Approve</th>
                    <th scope="col">Action</th>
                </tr>
                </thead>
                <tbody>
                @foreach($comments as $comment)
                    <tr>
                        <th scope="row">{{ $comment->id }}</th>
                        <td>{{ $comment->comment }}</td>
                        @if($comment->approve)
                            <td>approve</td>
                        @else
                            <td>
                                <form style="float: left; margin-right: 12px;"
                                      action="{{ route('admin.comments-approve', ['comment' => $comment]) }}"
                                      method="post">
                                    @csrf
                                    <button type="submit" class="btn btn-warning btn-sm active">Approve</button>
                                </form>
                            </td>

                        @endif
                        <td>
                            <div class="row">
                                <div class="col-6 box-btn">
                                    <a class="btn btn-outline-warning"
                                       href="{{ route('admin.comments.edit', ['comment' => $comment]) }}">Edit</a>
                                    <form style="float: left; margin-right: 12px;"
                                          action="{{ route('admin.comments.destroy', ['comment' => $comment]) }}"
                                          method="post">
                                        @method('delete')
                                        @csrf
                                        <button class="btn btn-danger btn-sm active">Remove</button>
                                    </form>
                                </div>
                            </div>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

        <div class="justify-content-md-center p-5">
            <div class="col-md-auto">
                {{ $comments->links() }}
            </div>
        </div>

    </div>

@endsection


