@extends('layouts.app')
@section('content')
    <div class="container mt-5">
        <div style="width: 80%; margin: 0 auto">
            <h2 class="mt-5">Update comment</h2>
            <form method="post" action="{{ route('admin.comments.update', ['comments' => $comment]) }}" enctype="multipart/form-data">
                @method('put')
                @csrf
                <div class="mb-3 mt-5" style="display: flex; align-items: start; gap: 10px;justify-content: center;">
                    <textarea type="text" name="text" class="form-control" placeholder="News text" aria-label="news_text"
                              aria-describedby="addon-wrapping">{{ $comment->comment }}</textarea>
                </div>
                @error('comments')
                <div class="alert alert-danger">{{$message}}</div>
                @enderror

                <div style="width: 15%;margin: 0 auto;">
                    <button type="submit" style="padding: 8px 50px; color: black" class="btn btn-primary">Update</button>
                </div>
            </form>
        </div>
    </div>

@endsection

