@extends('layouts.app')
@section('content')
    <div class="container mt-5">
        <h2>Admin profile</h2>

        <div class="row">
            <div class="col-4"><a href="{{ route('admin.news.index') }}" class="btn btn-light">News</a></div>
            <div class="col-4"><a href="{{ route('admin.comments.index') }}" class="btn btn-light">Comments</a></div>
            <div class="col-4"></div>
        </div>

    </div>

@endsection

