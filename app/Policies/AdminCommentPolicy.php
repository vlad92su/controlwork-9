<?php

namespace App\Policies;

use App\Models\User;

class AdminCommentPolicy
{

    /**
     * @param User $user
     * @return bool
     */
    public function index(User $user): bool
    {
        return $user->is_admin;
    }


    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->is_admin;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function store(User $user): bool
    {
        return $user->is_admin;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user): bool
    {
        return $user->is_admin;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function edit(User $user): bool
    {
        return $user->is_admin;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user): bool
    {
        return $user->is_admin;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function delete(User $user): bool
    {
        return $user->is_admin;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function approve(User $user): bool
    {
        return $user->is_admin;
    }
}
