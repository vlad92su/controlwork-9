<?php

namespace App\Policies;

use App\Models\User;

class AdminNewsPolicy
{
    /**
     * @param User $user
     * @return bool
     */
    public function index(User $user): bool
    {
        return $user->is_admin;
    }


    /**
     * @param User $user
     * @return bool
     */
    public function create(User $user): bool
    {
        return $user->is_admin;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function store(User $user): bool
    {
        return $user->is_admin;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function show(User $user): bool
    {
        return $user->is_admin;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function edit(User $user): bool
    {
        return $user->is_admin;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function update(User $user): bool
    {
        return $user->is_admin;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function delete(User $user): bool
    {
        return $user->is_admin;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function restore(User $user): bool
    {
        return $user->is_admin;
    }

    /**
     * @param User $user
     * @return bool
     */
    public function forceDelete(User $user): bool
    {
        return $user->is_admin;
    }
}
