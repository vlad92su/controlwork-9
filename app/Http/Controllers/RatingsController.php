<?php

namespace App\Http\Controllers;

use App\Http\Requests\RatingRequest;
use App\Models\Rating;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Http\RedirectResponse;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;

class RatingsController extends Controller
{

    /**
     * @param RatingRequest $request
     * @return Application|\Illuminate\Foundation\Application|RedirectResponse|Redirector
     */
    public function store(RatingRequest $request)
    {
        if(Auth::check()) {
            $rating = new Rating();
            $rating->quality = $request->input('quality');
            $rating->relevance = $request->input('relevance');
            $rating->satisfied = $request->input('satisfied');
            $rating->news_id = $request->input('news_id');
            $rating->user_id = auth()->user()->id;

            $rating->save();

            return redirect()->route('news.show', $rating->news_id)
                ->with('status', "Rating saved successfully!!!");
        }
        return redirect('login');

    }

}
