<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class CommentController extends Controller
{

    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', Comment::class);
        $comments = Comment::latest()->paginate(10);
        return view('admin.comments.index', compact('comments'));
    }


    /**
     * @param Comment $comment
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     * @throws AuthorizationException
     */
    public function edit(Comment $comment)
    {
        $this->authorize('edit', Comment::class);

        return view('admin.comments.edit', compact('comment'));
    }


    /**
     * @param CommentRequest $request
     * @param Comment $comment
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(CommentRequest $request, Comment $comment)
    {
        $this->authorize('update', Comment::class);
        $data = $request->all();
        $comment->update($data);

        return redirect()->route('admin.comments.index')
            ->with('status', "Comment update successfully!!!");
    }


    /**
     * @param Comment $comment
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('destroy', Comment::class);
        $comment->delete();
        return redirect()->action([self::class, 'index'])->with('status', "Comment successfully deleted!");
    }


    /**
     * @param Comment $comment
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function approve(Comment $comment)
    {
        $this->authorize('approve', Comment::class);
        $comment->approve = true;
        $comment->update();
        return redirect()->action([self::class, 'index'])->with('status', "Comment successfully approve!");
    }
}
