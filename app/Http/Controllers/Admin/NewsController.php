<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\NewsRequest;
use App\Models\Category;
use App\Models\News;
use App\Models\Tag;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;

class NewsController extends Controller
{

    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     * @throws AuthorizationException
     */
    public function index()
    {
        $this->authorize('index', News::class);
        $news = News::latest()->paginate(10);
        return view('admin.news.index', compact('news'));
    }


    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('create', News::class);
        $categories = Category::all();
        $tags = Tag::all();

        return view('admin.news.create', compact('categories', 'tags'));
    }


    /**
     * @param NewsRequest $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(NewsRequest $request)
    {
        $this->authorize('store', News::class);
        $news = new News();
        $news->text = $request->input('text');
        $news->category_id = $request->input('category_id');
        $news->tag_id = $request->input('tag_id');
        $news->user_id = auth()->user()->id;
        if($request->publication_date) {
            $news->publication_date = $request->input('publication_date');
        }
        $news->save();

        return redirect()->route('admin.news.index')
            ->with('status', "News created successfully!!!");
    }


    /**
     * @param News $news
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     * @throws AuthorizationException
     */
    public function edit(News $news)
    {
        $this->authorize('edit', News::class);
        $categories = Category::all();
        $tags = Tag::all();

        return view('admin.news.edit', compact('news', 'categories', 'tags'));
    }


    /**
     * @param NewsRequest $request
     * @param News $news
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function update(NewsRequest $request, News $news)
    {
        $this->authorize('update', News::class);
        $data = $request->all();
        $news->update($data);

        return redirect()->route('admin.news.index')
            ->with('status', "News update successfully!!!");
    }


    /**
     * @param News $news
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function destroy(News $news)
    {
        $this->authorize('destroy', News::class);
        $news->delete();
        return redirect()->action([self::class, 'index'])->with('status', "News successfully deleted!");
    }
}
