<?php

namespace App\Http\Controllers;

use App\Http\Requests\CommentRequest;
use App\Models\Comment;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;

class CommentsController extends Controller
{

    /**
     * @param CommentRequest $request
     * @return RedirectResponse
     */
    public function store(CommentRequest $request)
    {
        if(Auth::check()) {
            $comment = new Comment();
            $comment->comment = $request->input('comments');
            $comment->news_id = $request->input('news_id');
            $comment->user_id = auth()->user()->id;

            $comment->save();

            return redirect()->back()->with('status', "Your comments is being reviewed");
        }
        return redirect()->route('login');
    }

}
