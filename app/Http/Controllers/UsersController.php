<?php

namespace App\Http\Controllers;

use App\Models\News;
use App\Models\Rating;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;

class UsersController extends Controller
{

    /**
     * @param User $user
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function show(User $user)
    {
        $now = Carbon::now();

        $public_news = News::whereDate('publication_date', '<=', $now)
            ->where('user_id', $user->id)
            ->get();

        $results = Rating::where('user_id', $user->id)->get();

        $count_public_news = count($public_news);
        $count_quality = 0;
        $count_relevance = 0;

        foreach ($results as $result) {
            $count_quality += $result->quality;
            $count_relevance += $result->relevance;
        }

        $rating_news = round(($count_public_news + $count_quality + $count_relevance) / 3, 2);

        return view('users.show', compact('user', 'count_public_news','count_quality', 'count_relevance', 'rating_news'));
    }

}
