<?php

namespace App\Http\Controllers;

use App\Http\Requests\NewsRequest;
use App\Models\Category;
use App\Models\News;
use App\Models\Tag;
use Carbon\Carbon;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NewsController extends Controller
{

    /**
     * @param Request $request
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function index(Request $request)
    {

        $now = Carbon::now();
        $categories = Category::all();
        $tags = Tag::all();
        $newsQuery = News::whereDate('publication_date', '<=', $now)->latest();

        if ($request->isMethod('post')) {
            if ($request->category == "Filter by category") {
                $tag_id = $request->tag;
                $newsQuery->where('tag_id', $tag_id);
            } else {
                $category_id = $request->category;
                $newsQuery->where('category_id', $category_id);
            }
        }

        $news = $newsQuery->paginate(8);

        return view('news.index', compact('news', 'categories', 'tags'));

    }

    /**
     * @return Application|Factory|View|\Illuminate\Foundation\Application|RedirectResponse
     */
    public function create()
    {
        if(Auth::check()) {
            $categories = Category::all();
            $tags = Tag::all();

            return view('news.create', compact('categories', 'tags'));
        }
        return redirect()->route('login');
    }


    /**
     * @param NewsRequest $request
     * @return RedirectResponse
     */
    public function store(NewsRequest $request)
    {

        $news = new News();
        $news->text = $request->input('text');
        $news->category_id = $request->input('category_id');
        $news->tag_id = $request->input('tag_id');
        $news->user_id = auth()->user()->id;
        $news->save();

        return redirect()->route('news.index')
            ->with('status', "News created successfully!!!");

    }


    /**
     * @param News $news
     * @return Application|Factory|View|\Illuminate\Foundation\Application
     */
    public function show(News $news)
    {
        return view('news.show', compact('news'));
    }

}
